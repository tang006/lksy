lksy 是一个使用springmvc + shiro + redis + quartz + h-ui 搭建的一个基础项目框架 文档各方面比较全面方便大家学习。 

欢迎更多人加入QQ群 335102947

项目使用环境 jdk1.7+tomcat7.0+eclipse 

项目本地访问地址为 localhost:8080/sys/login 初始密码已经指定

项目当前演示地址 [http://lookseaeveryday.com/sys/login](http://lookseaeveryday.com/sys/login) 


**下面截图是使用超级管理员登录的，默认用户不具备**


首页展示效果如下图所示：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0316/182557_bd422dd0_376915.png "在这里输入图片标题")

登录后首页如下图所示：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163308_bbd7d4c9_376915.png "在这里输入图片标题")

城市管理模块包括 

1） 城市列表 

2） 线路列表

![输入图片说明](http://git.oschina.net/uploads/images/2016/0316/182917_c866ceaa_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0316/183006_ae055312_376915.png "在这里输入图片标题")

系统管理模块包括 

1）用户管理 

2）角色管理 

3）菜单管理 

4）权限管理

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163423_22d71f39_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163447_e05828c0_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163512_031b31a0_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163532_37aee69c_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163555_cef6225e_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163633_537dd3ce_376915.png "在这里输入图片标题")

子菜单的 添加删除 

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163658_b5ba06c6_376915.png "在这里输入图片标题")


页面具体功能的控制 通过点击左边的tree 动态展示功能权限


![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163717_8f667297_376915.png "在这里输入图片标题")


系统监控模块包括 

1） 日志管理 

2） 数据监控） 

3） 定时任务管理

日志管理包含日志的倒出功能支持 pdf 导出 word excel 等等

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/163932_fff75b23_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/164022_6c37a3aa_376915.png "在这里输入图片标题")

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/164105_a2f828a0_376915.png "在这里输入图片标题")


系统统计模块

支持当天访问量和一个星期访问量

![输入图片说明](http://git.oschina.net/uploads/images/2016/0506/164201_30347c5c_376915.png "在这里输入图片标题")
