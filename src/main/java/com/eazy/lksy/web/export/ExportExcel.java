package com.eazy.lksy.web.export;

import java.io.OutputStream;
import java.util.List;

import com.eazy.lksy.web.utils.ExportUtil;

public class ExportExcel extends Export {

	public static final String LOGMANAGER = "logManager"; // 日志管理

	private String[] getColumns(String type) {
		if (LOGMANAGER.equals(type)) {
			return new String[] { "ID", "浏览器类型", "操作系统", "IP地址", "创建时间" };
		} else {
			return null;
		}
	}

	@Override
	public OutputStream writeStream(OutputStream out, String type, List<String[]> records) {
		String[] columns = getColumns(type);
		records.add(0, columns);
		ExportUtil.writeStream(out, records);
		return out;
	}

}
