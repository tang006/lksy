package com.eazy.lksy.web.export;

import java.io.OutputStream;
import java.util.List;

public abstract class Export {

	public abstract OutputStream writeStream(OutputStream out, String type, List<String[]> records);
}
