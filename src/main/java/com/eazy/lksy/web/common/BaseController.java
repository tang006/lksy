package com.eazy.lksy.web.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.eazy.lksy.web.utils.ActionRequest;
import com.eazy.lksy.web.utils.StringEscapeEditor;
import com.eazy.lksy.web.view.PageData;

/**
 * @author jzx
 * @desc controller 基础操作类
 * @date 2016.3.19
 */
public abstract class BaseController {

	protected HttpServletRequest request;
	protected HttpServletResponse response;

	public String getPara(String key) {
		return request.getParameter(key);
	}

	private PageData getPageData() {
		return new PageData(this.getRequest());
	}

	public Map<String, String> getFormPage() {
		return getPageData().getColumns();
	}

	public String[] getParaValues(String key) {
		return request.getParameterValues(key);
	}

	public void setAttr(String key, Object value) {
		request.setAttribute(key, value);
	}

	public void setAttrs(Map<String, Object> map) {
		ActionRequest.setAttrs(request, map);
	}

	public Object getAttr(String key) {
		return request.getAttribute(key);
	}

	public void renderResult(boolean result) {
		if (result) {
			renderJson200();
		} else {
			renderJson500();
		}
	}

	public void renderJson200() {
		renderJson("{\"code\":200}");
	}

	public void renderJson500() {
		renderJson("{\"code\":500}");
	}

	public void renderJson(String data) {
		ActionRequest.renderJson(data, response);
	}

	public void renderWord(String fileName, String columnName, List<String[]> list1) {
		ActionRequest.renderWord(fileName, columnName, list1, response);
	}

	public void renderExcel(String fileName, String columnName, List<String[]> list1) {
		ActionRequest.renderExcel(fileName, columnName, list1, response);
	}

	public void renderPdf(String fileName, String columnName, List<String[]> list1) {
		ActionRequest.renderPdf(fileName, columnName, list1, response);
	}

	@InitBinder
	public void initBinder(ServletRequestDataBinder binder) {
		/**
		 * 自动转换日期类型的字段格式
		 */
		binder.registerCustomEditor(Date.class,new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));

		/**
		 * 防止XSS攻击
		 */
		binder.registerCustomEditor(String.class, new StringEscapeEditor(true, false));
	}

	@ModelAttribute
	private void init(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpSession getSession() {
		return request.getSession();
	}
}
