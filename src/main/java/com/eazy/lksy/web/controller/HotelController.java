package com.eazy.lksy.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.HotelService;

/**
 * @author jzx
 * @date 2016/3/30
 * @desc 酒店管理
 */
@Controller
@RequestMapping(value = "hotel")
public class HotelController  extends BaseController {

	@Autowired
	private HotelService hotelService;
	/**
	 * 酒店查询列表
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView list() {
		ModelAndView view = new ModelAndView("hotel/hotel_list"); 
		view.addObject("data", hotelService.getListMap("select * from hotel"));
		return view;
	}
	
	/**
	 * 跳转到房间图片
	 */
	@RequestMapping(value = "/toRoomImage/{id}", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView toRoomImage(@PathVariable String id) {
		ModelAndView view = new ModelAndView("hotel/room_image"); 
		view.addObject("data", hotelService.getListMap("SELECT ri.*,r.`name` FROM room r,room_image ri WHERE r.`id` = ri.`r_id`  AND r.`h_id`=" + id));
		return view;
	}
	
	
}
