package com.eazy.lksy.web.controller;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.model.User;
import com.eazy.lksy.web.redis.Redis;
import com.eazy.lksy.web.service.LogService;
import com.eazy.lksy.web.service.PermissionService;
import com.eazy.lksy.web.service.UserService;
import com.eazy.lksy.web.utils.DataUtil;
import com.eazy.lksy.web.utils.MD5;
import com.eazy.lksy.web.utils.StrKit;
import com.eazy.lksy.web.utils.UserUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * @author jzx
 * @desc 登录管理
 * @date 2016/2/29
 */
@Controller
@RequestMapping("/sys")
public class LoginController extends BaseController {

	@Resource
	private JmsTemplate jmsTemplate;
	@Autowired
	private LogService logService;
	@Autowired
	private UserService userService;
	@Autowired
	private PermissionService permissionService;

	/**
	 * 用户登录
	 */
	@RequestMapping(value = "/login", method = { RequestMethod.POST, RequestMethod.GET })
	public String login(RedirectAttributes redirectAttributes) {
		Map<String, String> map = getFormPage();
		HttpSession sessionCode = getSession();
		
		if(StrKit.isEmpty(map.get("name")) && StrKit.isEmpty(map.get("pwd")) ) {
			redirectAttributes.addFlashAttribute("msg", "用户名和密码不能为空");
		}
		else if(StrKit.isEmpty(map.get("name"))) {
			redirectAttributes.addFlashAttribute("msg", "用户名不能为空");
		}
		else if(StrKit.isEmpty(map.get("pwd"))) {
			redirectAttributes.addFlashAttribute("msg", "密码不能为空");
		} 
		 else {
//			String code = sessionCode.getAttribute("code").toString();
//			if (!code.equalsIgnoreCase(map.get("code"))) {
//				redirectAttributes.addFlashAttribute("msg", "验证码输入错误");
//			} else {
				try {
					UsernamePasswordToken token = new UsernamePasswordToken(map.get("name"),MD5.encodeString(map.get("pwd")));
					token.setRememberMe(true);
					Subject currentUser = SecurityUtils.getSubject();
					currentUser.login(token);
					// 验证是否登录成功
					if (currentUser.isAuthenticated()) {
						final String ip = DataUtil.getIpAddr(request);
						 //发送日志
				        jmsTemplate.send(new MessageCreator() {
				            @Override
				            public Message createMessage(Session session) throws JMSException {
				                MapMessage message = session.createMapMessage();
				                message.setString("type", "login");
				                message.setString("account", UserUtil.getCurrentUser().getName());
				                message.setString("ip", ip);
				                message.setString("logintime", String.valueOf(System.currentTimeMillis()));
				                return message;
				            }
				        });
						return "redirect:index";
					} else {
						token.clear();
					}
				} catch (org.apache.shiro.authc.AuthenticationException ex) {
					redirectAttributes.addFlashAttribute("msg", "用户名或者密码输入错误!!!");
				}
	//		}
		}
		return "redirect:error";
	}

	/**
	 * 用户登出
	 */
	@RequestMapping(value = "/logout", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView logout() {
		ModelAndView andView = new ModelAndView("login/login");
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return andView;
	}
	
	/**
	 * 修改登录密码
	 */
	@RequestMapping(value = "/forgetpwd" , method = RequestMethod.POST)
	public String updatePwd(@RequestBody User user) {
		userService.update(user);
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:/sys/login";
	}
	
	/**
	 * 跳转到欢迎页
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView welcome() {
		Gson gson = new Gson();
		
		List<Map<String,Object>> data = null;
		try{
			if(Redis.exists("welcome")) {
				String load = Redis.get("welcome");
				data = gson.fromJson(load, new TypeToken<List<Map<String,Object>>>(){}.getType());
			} else {
				data = logService.selectLog();
				String load = gson.toJson(data);
				Redis.put("welcome", load);
			}
		} catch (NullPointerException e) {
			data = logService.selectLog(); // 直接默认查询
		}
		ModelAndView model = new ModelAndView("index/welcome");
		model.addObject("data", data);
		model.addObject("count", data.size());
		
		model.addObject("ip", "");
		model.addObject("last_time", "");
		// 获取当前时间第一个登录用户
		if(data != null) {
			model.addObject("ip", data.get(0).get("IP"));
			model.addObject("last_time", data.get(0).get("tim"));
		} 
		
		return model;
	}

	/**
	 * 跳转到首页
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView index() {
		if(UserUtil.getCurrentUser() == null) {
			return new ModelAndView("redirect:/sys/login");
		}
		String value = UserUtil.getCurrentUser().getId().toString();
		List<Map<String,Object>> result = permissionService.permissionList(value);
		
		ModelMap map = new ModelMap();
		map.put("data", result);
		
		return new ModelAndView("index/index",map);
	}

	/**
	 * 跳转到登录失败页面
	 */
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public ModelAndView fail(ModelMap model) throws UnsupportedEncodingException {
		return new ModelAndView("login/login",model);
	}
	
	
	/**
	 * 跳转到修改密码页面
	 */
	@RequestMapping(value = "/forget" , method = RequestMethod.GET)
	public ModelAndView toUpdatePwd() {
		User value = UserUtil.getCurrentUser();
		
		ModelAndView modelAndView = new ModelAndView("admin/admin_pwd");
		modelAndView.addObject("user", value);
		return modelAndView;
	}

	
}
