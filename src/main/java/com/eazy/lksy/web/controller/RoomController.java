package com.eazy.lksy.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.RoomService;

/**
 * @author jzx
 * @date 2016/04/01
 * @desc 房间管理
 */
@Controller
@RequestMapping(value = "room")
public class RoomController extends BaseController {

	@Autowired
	private RoomService roomService;
	
	/**
	 * 房间查询列表
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.POST, RequestMethod.GET })
	public ModelAndView list() {
		ModelAndView view = new ModelAndView("room/room_list"); 
		view.addObject("data", roomService.getListMap(" SELECT r.*,h.hotel_name,h.img FROM room r,hotel h WHERE r.`h_id` = h.id"));
		return view;
	}
	
}
