package com.eazy.lksy.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.eazy.lksy.web.common.BaseController;
import com.eazy.lksy.web.service.AreaService;

/**
 * @author jzx
 * @date 2016/2/15
 * @desc 区域管理
 */
@Controller
@RequestMapping("/area")
public class AreaController extends BaseController {

	@Autowired
	private AreaService areaService;
	
	/**
	 * 区域查询
	 */
	@RequestMapping(value = "list/{id}" , method = RequestMethod.GET)
	public ModelAndView selectArea(@PathVariable String id) {
		ModelAndView andView = new ModelAndView("city/area/area_list");
		List<Map<String, Object>> data = areaService.selectArea(id);
		andView.addObject("data", data);
		andView.addObject("city_id", id);
		return andView;
	}
	

	/**
	 * 区域添加
	 */
	@RequestMapping(value = "addArea" , method = RequestMethod.POST)
	public String addArea(HttpServletRequest request) {
		String name = getPara("name");
		String cityId = getPara("city_id");
		areaService.addArea(name, cityId);
		return "redirect:/area/list/" + cityId;
	}

	/**
	 * 区域修改
	 */
	@RequestMapping(value = "updateArea" , method = RequestMethod.POST)
	public String updateArea() {
		Map<String, String> param = getFormPage();
		areaService.updateArea(param);
		return "redirect:/area/list/" + param.get("cityid");
	}

	/**
	 * 区域删除
	 */
	@RequestMapping(value = "deleteArea/{id}/{cityid}" , method = RequestMethod.GET)
	public String deleteArea(@PathVariable String id,@PathVariable String cityid) {
		areaService.deleteArea(id);
		return "redirect:/area/list/" + cityid;
	}

	/**
	 * 跳转到区域添加
	 */
	@RequestMapping(value = "toAddarea/{id}" , method = RequestMethod.GET)
	public ModelAndView toAddarea(@PathVariable String id) {
		return new ModelAndView("city/area/area_add","city_id",id);
	}

	/**
	 * 跳转到区域修改
	 */
	@RequestMapping(value = "toUpdatearea/{id}" , method = RequestMethod.GET)
	public ModelAndView toUpdatearea(@PathVariable String id) {
		ModelAndView andView = new ModelAndView("city/area/area_update");
		Map<String, Object> result = areaService.findViewById("area", id);
		andView.addObject("data", result);
		return andView;
	}
}
