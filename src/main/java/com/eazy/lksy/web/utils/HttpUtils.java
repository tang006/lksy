package com.eazy.lksy.web.utils;

import java.io.IOException;
import java.net.URI;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * @author jzx
 * @date 2016.2.25
 */
public class HttpUtils {

	public static void getUrl() {
		request(new Get(""));
	}
	
	private static void request(HttpRequestBase base) {
		CloseableHttpClient client = HttpClientBuilder.create().build();
		CloseableHttpResponse response = null ;
		try {
			if(base instanceof Get) {
				Get get =(Get) base;
				response = client.execute(get);
			}
			if(base instanceof Post) {
				
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

class Get extends HttpGet {
	public Get(final String url) {
		super(url);
	}
	
	public Get(final URI uri) {
		super(uri);
	}
}

class Post extends HttpPost {
	
}