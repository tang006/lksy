package com.eazy.lksy.web.redis;

import java.util.Map;
import java.util.Set;

import com.eazy.lksy.web.utils.RedisUtil;

@Deprecated
public final class Redis {
   
    public static void put(String key,String value) {
    	put(key, (Object)value);
    }
    
    public static void put(String key,String value, int exprise) {
    	put(key, (Object)value);
    	put(key, exprise);
    }
    
    public static void put(String key,Object value) {
    	RedisUtil.getJedis().set(key, value + "");
    }
    
    public static void put(String key,Map<String,String> value) {
    	RedisUtil.getJedis().hmset(key, value);
    }

    public static void put(String key,int exprise) {
    	RedisUtil.getJedis().expire(key, exprise);
    }
    public static String get(String key) {
    	return RedisUtil.getJedis().get(key);
    } 
    
    public static Set<String> getKeys() {
    	return RedisUtil.getJedis().keys("*");
    }
    
    public static void remove(String key) {
    	RedisUtil.getJedis().del(key);
    }
    
    public static void removeAll() {
    	RedisUtil.getJedis().flushDB();
    }
    
    public static boolean exists(String key) {
    	return RedisUtil.getJedis().exists(key);
    }
    
}