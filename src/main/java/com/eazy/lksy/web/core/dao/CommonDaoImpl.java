package com.eazy.lksy.web.core.dao;

import java.util.List;
import java.util.Map;

public class CommonDaoImpl extends BaseDao implements CommonDao {

	@Override
	public Map<String, Object> findViewById(String tableName, String id) {
		return super.findViewById(tableName, id);
	}

	@Override
	public List<Map<String, Object>> getListMap(String sql) {
		return dao.queryForList(sql);
	}

	@Override
	public Map<String, Object> getMap(String sql) {
		return dao.queryForMap(sql);
	}

}
